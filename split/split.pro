TEMPLATE = app
TARGET = split

QT = qml quick dbus gui-private
CONFIG += c++11 link_pkgconfig wayland-scanner pkgdatadir
#DESTDIR = .
DESTDIR = $${OUT_PWD}/../package/root/bin
PKGCONFIG += wayland-client

SOURCES += src/main.cpp src/shell-desktop.cpp
HEADERS += src/shell-desktop.h

RESOURCES += qml/qml.qrc images/images.qrc

WAYLANDCLIENTSOURCES += \
    protocol/agl-shell-desktop.xml
