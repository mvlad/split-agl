import QtQuick 2.2
import QtQuick.Window 2.11
import QtQuick.Layouts 1.1


Window {
    id: page
    flags: Qt.FramelessWindowHint
    visible: true

    width: Screen.width
    height: Screen.height

    color: "#474747"
}
//    // Make a ball to bounce
//    Rectangle {
//        id: ball
//
//        // Add a property for the target y coordinate
//        property variant direction : "right"
//
//        x: 20; width: 20; height: 20; z: 1
//        color: "blue"
//
//        // Move the ball to the right and back to the left repeatedly
//        SequentialAnimation on x {
//            loops: Animation.Infinite
//            NumberAnimation { to: page.width - 40; duration: 2000 }
//
//            PropertyAction { target: ball; property: "direction"; value: "left" }
//
//            NumberAnimation { to: 20; duration: 2000 }
//            PropertyAction { target: ball; property: "direction"; value: "right" }
//        }
//
//        // Make y move with a velocity of 200
//        Behavior on y { SpringAnimation{ velocity: 200; }
//        }
//
//        Component.onCompleted: y = page.height-10 // start the ball motion
//
//        // Detect the ball hitting the top or bottom of the view and bounce it
//        onYChanged: {
//	    console.log("onYChanged, y=" + y + ", page.height " + page.height)
//            if (y <= 0) {
//                y = page.height - 20;
//            } else if (y >= page.height - 20) {
//                y = 0;
//            }
//        }
//
//        onXChanged: {
//	    console.log("onXChanged, x=" + x + ", page.width " + page.width)
//
//            if (x <= 0) {
//                //x = page.width - 20;
//            } else if (x >= page.width - 20) {
//                x = 0
//            }
//        }
//    }
